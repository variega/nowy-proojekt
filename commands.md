# Przydatne polecenia GIT

## basics

* **git init** - inicjalizuje repozytorium GIT w katalogu
* **git clone {adres repozytorium}** - klonuje repozytorium do katalogu
* **git status** - pokazuje status repozytorium (pokazuje informację o zmodyfikowanych, nowych, usuniętych oraz nie należące do repozytorium plikach)
* **git add {ścierzka do pliku}** - dodaje plik do repozytorium (np. `git add folder/plik.php`)
* **git add -A** - dodaje wszystkie nie należące do repozytorium pliki
* **git config --global color.ui auto** - włącza koloryzowanie wyników w konsoli
* **git config --global core.pager '{nazwa}'** - ustawia program do przeglądania logów (brak w konsoli)

## repop

* **git fetch -p** - kasuje branche już nie istniejąca na głównym repo
* **git fetch {nazwa remota}** - pobiera listę zmian z innego repozytorium (w tym pokazuje nowe gałęzie)
* **git remote -v** - lista repo
* **git remote remove {repo}** - usuwa wskazane repo
* **git remote set-url {nazwa repo} {url}** - zmienia adres dla podanego repo
* **git remote add {jakaś nazwa} {adres repozytorium}** - dodaje repozytorium innego użytkownika (`git remote add upstream https://github.com/bluetree-service/idylla.git`)
* **git remote -v** lista wszystkich zewnetrznych repozytoriów
* **git remote rm {nazwa dla remota}** - usuwa zewnętrzne repozytorium
* **git pull** - pobiera zmiany z aktualnej gałęzi
* **git pull {nazwa gałęzi}** - pobiera zmiany z wybranej gałęzi
* **git pull {nazwa remota} {nazwa gałęzi}** - pobiera zmiany z wybranej gałęzi wybranego zewnętrznego repozytorium
* **git pull --all --prune** - ściąga zmiany z repo + kasuje nieużywane branche
* **git pull --tags** - ściąga tagi
* **git pull --rebase** - 
* **git push** - wypycha zmiany na aktualnie wybraną gałąź
* **git push {nazwa gałęzi}** - wypycha zmiany na wskazaną gałąź
* **git push {nazwa remota} {nazwa gałęzi}** - wypycha zmiany na gałąź wskazanego repozytorium
* **git push --tags** - wysyła tagi na repo
* **git revert -- {plik}** - revert pojedyńczego pliku
* **git reset --soft HEAD^** - cofa zmiany bez usuwania dodanych plików
* **git reset --soft {numer commita}** - cofa zmiany bez usuwania dodanych plików do wskazanego commita (`git reset --soft b87dcea`)
* **git reset --hard {numer commita}** - cofa zmiany włącznie z usunięciem plików do wskazanego commita (`git reset --hard b87dced`)
* **git reset --merge ORIG_HEAD** - resetuje zmiany z ostatniego merg-a
* **git checkout -- {plik}** - przywraca oryginalny plik
* **git checkout {commit} -- {plik}** - przywraca stan pliku ze wskazanego commita
* **git checkout -b {nazwa brancha} origin {nazwa brancha}** - tworzy brancha lokalnie i pobiera go (z aktualnymi zmianami) ze zdalnego miejsca
* **git ls-files** - lista plików z ich ścierzkami w repo (-md + zmodyfikowane i usunięte)
* **git rm --cached** - usuwa z pliki z listy do commitów
* **git rm {plik}** - kasuje z repo plik
* **git rm {plik}** - usuwa plik z repo
